from flask import request
from flask_restful import Resource
from flask_jwt_extended import jwt_required
from members.api.schemas import UserSchema
from members.models import User
from members.extensions import db
from members.commons.pagination import paginate
from requests import get
URL = "https://jsonplaceholder.typicode.com/posts"

class MemberResource(Resource):
    """Single object resource

    ---
    get:
      tags:
        - api
      parameters:
        - in: path
          name: member_id
          schema:
            type: integer
      responses:
        200:
          content:
            application/json:
              schema:
                type: object
                properties:
                  userId:
                   type: int
                   example: 1
                  id:
                   type: int
                   example: 1
                  title:
                   type: string
                   example: "sunt aut facere repellat provident occaecati excepturi optio reprehenderit"
                  body:
                   type: string
                   example: "quia et suscipit suscipit recusandae consequuntur expedita et cum reprehenderit molestiae ut ut quas totam nostrum rerum est autem sunt rem eveniet architecto"

        404:
          description: user does not exists
    """

    method_decorators = [jwt_required()]

    def get(self, member_id):
        response = get(URL).json()
        _filter = [i for i in response if i["id"] == member_id ]
        return _filter[0] if _filter else {}



class MemberList(Resource):
    """get_all

    ---
    get:
      tags:
        - api
      responses:
        200:
          content:
            application/json:
              schema:
                allOf:
                  - $ref: '#/components/schemas/PaginatedResult'
                  - type: object
                    properties:
                      results:
                        type: array

    """

    method_decorators = [jwt_required()]

    def get(self):
        return get(URL).json()
