# MEMBErs API

Desarrollar una aplicación WEB, que cumpla con lo siguiente:
1. Que permita registrar y iniciar sesión a "Usuarios".
2. Que contenga una ruta (/members_only), solo accesible por usuarios validos.
3. Que la ruta mencionada en el punto 2, te permita seleccionar un entero, y te muestre la información del post correspondiente a ese ID.
   API para extraer la información: https://jsonplaceholder.typicode.com/posts
4. La aplicación tiene que estar desarrollada usando Python y Flask, se puede utilizar extensiones.

#Install
```
pip install -r requirements.txt
pip install -e .
flask db upgrade
flask members init
fask run
```

# for help
```
flask --help
```

# how to use

You can check the swagger documentation, by default we create the **user admin** with **password admin**
* `/swagger.json`: return OpenAPI specification file in json format
* `/swagger-ui`: swagger UI configured to hit OpenAPI json file

## for example
`htp://localhost:5000/swagger-ui`

#NOTA NO QUIZE CONSUMIR TIEMPO EN UN FRONTEND QUE CONSUMA LA API, PERO TENGO OTROS PROJECTOS QUE LO HACEN